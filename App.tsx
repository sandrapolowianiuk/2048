/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/emin93/react-native-template-typescript
 *
 * @format
 */

import React, {Fragment} from 'react';
import {Provider} from 'react-redux';
import GameScreen from "./src/Layout";
import store from "./src/Core/store";

const App = () => {
    return (
        <Provider store={store}>
            <Fragment>
                <GameScreen/>
            </Fragment>
        </Provider>
    );
};

export default App;
