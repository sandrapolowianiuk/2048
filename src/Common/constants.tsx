export const GAME_BOARD_SIZE = 4;
export const WIN_VALUE = 2048;
export const TO_RIGHT = 'TO_RIGHT';
export const TO_LEFT = 'TO_LEFT';
export const TO_TOP = 'TO_TOP';
export const TO_BOTTOM = 'TO_BOTTOM';

export const DIRECTIONS_VALUE = {
    TO_RIGHT: -1,
    TO_LEFT: 1,
    TO_TOP: 1,
    TO_BOTTOM: -1
};
