import {StyleSheet} from 'react-native';
import {RFValue} from "react-native-responsive-fontsize";

const styles = StyleSheet.create({
    title: {
        fontSize: RFValue(20),
        textAlign:'center',
        fontWeight:'bold'
    },
    description: {
        fontSize: RFValue(15),
        textAlign:'center',
    }
});

export default styles;

