import {StyleSheet} from "react-native";


const styles= StyleSheet.create({
    shadow:{
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.5,
        shadowRadius: 6.68,

        elevation: 11,
    }
});
export default styles;
