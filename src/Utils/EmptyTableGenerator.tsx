import {GAME_BOARD_SIZE} from "../Common/constants";

export const generateEmptyBoard = () => {
    let gameBoard: Cell[][] = new Array();
    let key = 0;

    for (let line = 0; line < GAME_BOARD_SIZE; line++) {
        let array = new Array();
        for (let column = 0; column < GAME_BOARD_SIZE; column++) {
            const cell: Cell = {key: key, value:null, merged:false};
            array.push(cell);
            key++;
        }
        gameBoard.push(array);
    }

    console.log(gameBoard[3][0].key);

    const keyForInitialTile = Math.floor(Math.random() * 15);

    gameBoard.filter(cellRow => {
        cellRow.filter(cell => {
            return cell.key === keyForInitialTile ? cell.value = 2 : cell;
        });
    });

    return gameBoard;
};


export default generateEmptyBoard;
