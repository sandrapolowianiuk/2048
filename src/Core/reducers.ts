import { combineReducers } from 'redux';
import gameBoard from '../Components/reducer'

const reducers = combineReducers({
    gameBoard
});

export default reducers;
