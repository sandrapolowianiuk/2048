import React, {Component} from "react";
import {View} from "react-native";
import style from "./content/style"
import EmptyCell from "../Cell/Cell";

type Props = {
    row: Cell[]
}
const CellRow = (props: Props) => {

    return (
        <View style={style.row}>
            {props.row ?
                props.row.map(cell => {
                    return <EmptyCell cell={cell}/>
                }) :
                null}
        </View>
    )
};


export default CellRow;
