import {StyleSheet} from "react-native";
import colors from "../../../Common/colors";


const styles = StyleSheet.create({
    gameContainer: {
        flex: 13,
        alignContent: 'center',
        justifyContent: 'center',
        padding: 10,
    },
    gameTable: {
        backgroundColor: colors.playField,
        flex: 1,
        borderRadius: 5,
        flexDirection: 'column',
        paddingRight: 1,
        paddingLeft: 1,
    },
});
export default styles;
