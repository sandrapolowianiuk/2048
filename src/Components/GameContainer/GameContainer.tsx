import React, {Component} from "react";
import {PanResponderGestureState, View} from "react-native";
import GestureRecognizer from 'react-native-swipe-gestures';
import {connect} from "react-redux";
import styles from "./Content/styles"
import CellRow from "../CellRow/Row";
import {updateCellByKey, updateCells, updateScore, setEndGame, setWin} from '../actions';
import commonStyles from '../../Common/styles'
import {
    DIRECTIONS_VALUE,
    GAME_BOARD_SIZE,
    TO_BOTTOM,
    TO_LEFT,
    TO_RIGHT,
    TO_TOP,
    WIN_VALUE
} from "../../Common/constants";
import WinDialog from "../Dialogs/WinDialog";

type Props = {
    cells: Cell[][];
    updateCellByKey: (newTile: NewTile) => void;
    updateCells: (cells: Cell[][]) => void;
    updateScore: (score: number) => void;
    setEndGame: () => void;
};

const numberArray: number[] = [];

const initialState = {
    emptyCellsKeys: numberArray,
    cellsMoved: false,
    win: false
};

type State = typeof initialState;

class GameContainer extends Component<Props, State> {
    state = initialState;

    addNewTile = () => {
        this.getEmptyTiles();
        const keyForNewTile = Math.floor(Math.random() * this.state.emptyCellsKeys.length);
        let randomValue = this.generateRandomValue();
        this.props.updateCellByKey({key: this.state.emptyCellsKeys[keyForNewTile], value: randomValue});
        this.state.cellsMoved = false;
        if (!this.isAnyMove())
            this.props.setEndGame();
    };

    getEmptyTiles = () => {
        this.state.emptyCellsKeys = [];
        this.props.cells.forEach(cellRow => {
            cellRow.forEach(cell => {
                if (!cell.value) {
                    this.state.emptyCellsKeys.push(cell.key);
                }
            })
        });
    };

    generateRandomValue = (): number => {
        return Math.random() < 0.9 ? 2 : 4
    };

    isAnyMove = (): boolean => {
        this.getEmptyTiles();
        if (this.state.emptyCellsKeys.length > 0)
            return true;
        if (this.canMerge())
            return true;
        return false;
    };

    canMerge = (): boolean => {
        for (let line = 0; line < GAME_BOARD_SIZE; line++) {
            for (let column = 0; column < GAME_BOARD_SIZE - 1; column++) {
                const currentCellValue = this.props.cells[line][column].value;
                if (line + 1 < GAME_BOARD_SIZE)
                    if (currentCellValue === this.props.cells[line + 1][column].value)
                        return true;
                if (currentCellValue === this.props.cells[line][column + 1].value)
                    return true;
            }
        }
        return false;
    };

    onSwipeUp(gestureState: PanResponderGestureState) {
        this.moveTiles(TO_TOP);
    }

    onSwipeDown(gestureState: PanResponderGestureState) {
        this.moveTiles(TO_BOTTOM);
    }

    onSwipeLeft(gestureState: PanResponderGestureState) {
        this.moveTiles(TO_LEFT);
    }

    onSwipeRight(gestureState: PanResponderGestureState) {
        this.moveTiles(TO_RIGHT);
    }

    moveTiles = (directionName: string) => {
        if (this.props.cells) {
            // @ts-ignore
            const direction = DIRECTIONS_VALUE[directionName];
            let cellsInMove = this.props.cells;

            if (directionName === TO_BOTTOM || directionName === TO_TOP)
                cellsInMove = this.transpose(cellsInMove);

            cellsInMove.filter(row => {
                return direction === -1 ? this.moveCellsToRight(row) : this.moveCellsToLeft(row);
            });

            if (directionName === TO_BOTTOM || directionName === TO_TOP)
                cellsInMove = this.transpose(cellsInMove);

            if (this.state.cellsMoved) {
                this.props.updateCells(cellsInMove);
                this.checkIfWin(cellsInMove);
                this.addNewTile();
            }
        }
    };

    transpose = (array: Cell[][]) => {
        return array[0].map((col, i) => array.map(row => row[i]));
    };

    moveCellsToRight = (row: Cell[]): Cell[] => {
        let startIndex = GAME_BOARD_SIZE - 2;
        const endIndex = -1;

        row.map(cell => {
            return cell.merged = false
        });

        while (startIndex != endIndex) {
            let i = startIndex + 1;
            let indexOfCurrentCell = startIndex;
            let next = false;
            while (i < GAME_BOARD_SIZE && !next) {
                if (row[indexOfCurrentCell].value)
                    if (!row[i].value) {
                        row[i].value = row[indexOfCurrentCell].value;
                        row[indexOfCurrentCell].value = null;
                        indexOfCurrentCell = i;
                        this.state.cellsMoved = true;
                    } else {
                        if (!row[i].merged && row[i].value === row[indexOfCurrentCell].value) {
                            next = true;
                            row = this.mergeInRow(row, i, indexOfCurrentCell);
                        } else {
                            next = true;
                        }
                    }
                i++;
            }
            startIndex--;
        }
        return row;
    };

    moveCellsToLeft = (row: Cell[]): Cell[] => {
        let startIndex = 1;

        row.map(cell => {
            cell.merged = false
        });

        while (startIndex != GAME_BOARD_SIZE) {
            let i = startIndex - 1;
            let indexOfCurrentCell = startIndex;
            let next = false;
            while (i > -1 && !next) {
                if (row[indexOfCurrentCell].value)
                    if (!row[i].value) {
                        row[i].value = row[indexOfCurrentCell].value;
                        row[indexOfCurrentCell].value = null;
                        indexOfCurrentCell = i;
                        this.state.cellsMoved = true;
                    } else {
                        if (!row[i].merged && row[i].value == row[indexOfCurrentCell].value) {
                            row = this.mergeInRow(row, i, indexOfCurrentCell);
                            next = true;
                        } else {
                            next = true;
                        }
                    }
                i--;
            }
            startIndex++;
        }
        return row;
    };

    mergeInRow = (row: Cell[], i: number, indexOfCurrentCell: number): Cell[] => {
        //@ts-ignore
        row[i].value = row[i].value * 2;
        row[i].merged = true;
        //@ts-ignore
        this.props.updateScore(row[i].value);
        row[indexOfCurrentCell].value = null;
        this.state.cellsMoved = true;
        return row;
    };

    checkIfWin = (cells: Cell[][]) => {
        if (!this.state.win)
            cells.filter(cellRow => {
                cellRow.filter(cell => {
                    if (cell.value === WIN_VALUE) {
                        this.setState({win: true})
                    }
                });
            });
    };

    render() {
        const config = {
            velocityThreshold: 0.3,
            directionalOffsetThreshold: 80
        };

        return (
            //@ts-ignore
            <GestureRecognizer
                onSwipeUp={(state) => this.onSwipeUp(state)}
                onSwipeDown={(state) => this.onSwipeDown(state)}
                onSwipeLeft={(state) => this.onSwipeLeft(state)}
                onSwipeRight={(state) => this.onSwipeRight(state)}
                config={config}
                style={styles.gameContainer}>
                <View style={[styles.gameTable, commonStyles.shadow]}>
                    {this.props.cells.map(row => {
                        return <CellRow row={row}/>
                    })}
                </View>
                <WinDialog win={this.state.win} resetWin={() => this.setState({win: false})}/>
            </GestureRecognizer>
        )
    }
}


type StateProps = {
    gameBoard: {
        cells: Cell[][];
        win: boolean;
    };
};
const mapStateToProps = ({gameBoard: {cells, win}}: StateProps) => ({
    cells,
    win
});

const mapDispatchToProps = {
    updateCellByKey,
    updateCells,
    updateScore,
    setEndGame,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(GameContainer);
