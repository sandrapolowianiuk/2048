import {createActions} from 'redux-actions';
import {UPDATE_SCORE, UPDATE_CELL_BY_KEY, UPDATE_CELLS, SET_END_GAME, START_NEW_GAME, SET_WIN} from "./constants";

export const {
    updateCells,
    updateCellByKey,
    updateScore,
    setEndGame,
    startNewGame,
    setWin
} = createActions(
    UPDATE_CELLS,
    UPDATE_CELL_BY_KEY,
    UPDATE_SCORE,
    SET_END_GAME,
    START_NEW_GAME,
    SET_WIN
);
