import {View} from 'react-native';
import Dialog from "react-native-dialog";
import React, {Component} from 'react';
import {connect} from "react-redux";
import {startNewGame, updateCells} from "../actions";
import generateEmptyBoard from "../../Utils/EmptyTableGenerator";
import styles from "../../Common/dialogStyle";

type Props = {
    win: boolean,
    resetWin:()=>void,
    startNewGame: () => void,
    updateCells: (cells: Cell[][]) => void
}

const initialState = {
    continue: false
};

type State = typeof initialState;

class WinDialog extends Component<Props, State> {
    state = initialState;

    newGame = () => {
        this.props.startNewGame();
        this.props.updateCells(generateEmptyBoard());
    };

    continueGame = () => {
        this.setState({continue: true})
    };

    render() {
        return (
            <View>
                < Dialog.Container visible={this.props.win && !this.state.continue}>
                    < Dialog.Title style={styles.title}> You win! </Dialog.Title>
                    <Dialog.Description style={styles.description}>
                        CONGRATULATIONS
                    </Dialog.Description>
                    <Dialog.Button label="Continue game" onPress={this.continueGame}/>
                    <Dialog.Button label="Start new game" onPress={this.newGame}/>
                </Dialog.Container>
            </View>
        )
    }
}


const mapDispatchToProps = {
    startNewGame,
    updateCells
};

export default connect(
    null,
    mapDispatchToProps
)(WinDialog);
