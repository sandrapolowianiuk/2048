import {View} from 'react-native';
import Dialog from "react-native-dialog";
import React from 'react';
import {connect} from "react-redux";
import {startNewGame, updateCells} from "../actions";
import generateEmptyBoard from "../../Utils/EmptyTableGenerator";
import styles from "../../Common/dialogStyle";

type Props = {
    score: number,
    gameOver: boolean,
    startNewGame: () => void,
    updateCells: (cells: Cell[][]) => void
}

const GameOverDialog: React.FC<Props> = (props: Props) => {

    const newGame = () => {
        props.startNewGame();
        props.updateCells(generateEmptyBoard());
    };

    return (
        <View>
            < Dialog.Container visible={props.gameOver}>
                < Dialog.Title style={styles.title}> Game Over </Dialog.Title>
                <Dialog.Description>
                    {`Your score: ${props.score}`}
                </Dialog.Description>
                <Dialog.Button label="Start new game" onPress={newGame}/>
            </Dialog.Container>
        </View>
    );
};

type StateProps = {
    gameBoard: {
        score: number;
        gameOver: boolean;
    };
};
const mapStateToProps = ({gameBoard: {score, gameOver}}: StateProps) => ({
    score,
    gameOver
});

const mapDispatchToProps = {
    startNewGame,
    updateCells
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(GameOverDialog);
