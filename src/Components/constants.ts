export const UPDATE_CELLS = "UPDATE_CELLS";
export const UPDATE_CELL_BY_KEY = 'UPDATE_CELL_BY_KEY';
export const UPDATE_SCORE = 'UPDATE_SCORE';
export const SET_END_GAME = 'SET_END_GAME';
export const START_NEW_GAME = 'START_NEW_GAME';
export const SET_WIN = 'SET_WIN';
