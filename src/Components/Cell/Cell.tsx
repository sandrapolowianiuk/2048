import React, {Component} from "react";
import {View, Text} from "react-native";
import style from "./content/styles"

type Props = {
    cell: Cell;
}

const EmptyCell = (props: Props) => {

    if (props.cell.value) {
        const color = props.cell.value <= 32768 ? 'tile' + props.cell.value : 'tile32768';
        return (
            // @ts-ignore
            <View style={[style[color], style.tile]}>
                <Text style={style.textTile}>
                    {props.cell.value}
                </Text>
            </View>
        )
    } else {
        return (
            <View style={style.cell}/>
        )
    }
};

export default EmptyCell;
