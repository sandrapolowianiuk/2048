import {StyleSheet} from 'react-native';
import colors from "../../../Common/colors";
import {RFValue} from "react-native-responsive-fontsize";

const styles = StyleSheet.create({
    cell: {
        flex: 1,
        backgroundColor: colors.emptyCell,
        margin: 3,
        borderRadius: 8
    },
    textTile: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: RFValue(25),
        color: 'white'
    },
    tile: {
        justifyContent: 'center'
    },
    tile2: {
        flex: 1,
        backgroundColor: colors.blue2,
        margin: 3,
        borderRadius: 8
    },
    tile4: {
        flex: 1,
        backgroundColor: colors.blue4,
        margin: 3,
        borderRadius: 8
    },
    tile8: {
        flex: 1,
        backgroundColor: colors.blue8,
        margin: 3,
        borderRadius: 8
    },
    tile16: {
        flex: 1,
        backgroundColor: colors.blue16,
        margin: 3,
        borderRadius: 8
    },
    tile32: {
        flex: 1,
        backgroundColor: colors.blue32,
        margin: 3,
        borderRadius: 8
    },
    tile64: {
        flex: 1,
        backgroundColor: colors.blue64,
        margin: 3,
        borderRadius: 8
    },
    tile128: {
        flex: 1,
        backgroundColor: colors.blue128,
        margin: 3,
        borderRadius: 8
    },
    tile256: {
        flex: 1,
        backgroundColor: colors.blue256,
        margin: 3,
        borderRadius: 8
    },
    tile512: {
        flex: 1,
        backgroundColor: colors.blue512,
        margin: 3,
        borderRadius: 8,
        color: colors.brightText,
    },
    tile1024: {
        flex: 1,
        backgroundColor: colors.blue1024,
        margin: 3,
        borderRadius: 8,
        color: colors.brightText,
    },
    tile2048: {
        flex: 1,
        backgroundColor: colors.blue2048,
        margin: 3,
        borderRadius: 8,
        color: colors.brightText,
    },
    tile4096: {
        flex: 1,
        backgroundColor: colors.blue4096,
        margin: 3,
        borderRadius: 8,
        color: colors.brightText,
    },
    tile8192: {
        flex: 1,
        backgroundColor: colors.blue8192,
        margin: 3,
        borderRadius: 8,
        color: colors.brightText,
    },
    tile16384: {
        flex: 1,
        backgroundColor: colors.blue16384,
        margin: 3,
        borderRadius: 8,
        color: colors.brightText,
    },
    tile32768: {
        flex: 1,
        backgroundColor: colors.blue32768,
        margin: 3,
        borderRadius: 8,
        color: colors.brightText,
    },
});

export default styles;

