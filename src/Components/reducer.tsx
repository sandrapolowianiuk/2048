import {Action, handleActions} from 'redux-actions';
import {SET_END_GAME, SET_WIN, START_NEW_GAME, UPDATE_CELL_BY_KEY, UPDATE_CELLS, UPDATE_SCORE} from "./constants";

export type GameReducer = {
    cells: Cell[][],
    score: number,
    gameOver: boolean,
}

const initialState: GameReducer = {
    cells: [],
    score: 0,
    gameOver: false,
};


export default handleActions<GameReducer, any>(
    {
        [UPDATE_CELLS]: (state: GameReducer = initialState, action: Action<Cell[][]>) => ({
            ...state,
            cells: action.payload,
        }),
        [UPDATE_CELL_BY_KEY]: (state = initialState, action: Action<NewTile>) => ({
                ...state,
                cells: state.cells.filter(cellRow => {
                    cellRow.filter(cell => {
                        return cell.key === action.payload.key ? cell.value = action.payload.value : cell;
                    });
                    return cellRow;
                })
            }
        ),
        [UPDATE_SCORE]: (state: GameReducer = initialState, action: Action<number>) => ({
            ...state,
            score: state.score + action.payload
        }),
        [SET_END_GAME]: (state: GameReducer = initialState) => ({
            ...state,
            gameOver: !state.gameOver
        }),
        [START_NEW_GAME]: (state: GameReducer = initialState) => {
            return initialState;
        },
    },
    initialState
);
