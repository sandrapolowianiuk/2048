import React, {Component} from 'react';
import {View} from 'react-native';
import styles from './Style/style'
import Header from "./Header/Header";
import Footer from "./Footer/Footer";
import GameContainer from "../Components/GameContainer/GameContainer";
import {updateCells} from "../Components/actions";
import generateEmptyBoard from "../Utils/EmptyTableGenerator";
import {connect} from "react-redux";
import GameOverDialog from "../Components/Dialogs/GameOverDialog";

type Props = {
    updateCells: (cells: Cell[][]) => void;
}

class GameScreen extends Component<Props> {

    componentWillMount(): void {
        this.props.updateCells(generateEmptyBoard())
    }

    render() {
        console.disableYellowBox = true;
        return (
            <View style={styles.container}>
                <Header/>
                <GameContainer/>
                <Footer/>
                <GameOverDialog/>
            </View>
        );
    }
}

const mapDispatchToProps = {
    updateCells
};

export default connect(
    null,
    mapDispatchToProps
)(GameScreen);
