import {StyleSheet} from "react-native";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import colors from "../../../Common/colors"

const styles= StyleSheet.create({
    header:{
        flex:5,
        flexDirection: 'row',
    },
    gameName:{
        textAlign: 'center',
        fontSize: RFValue(40),
        fontWeight: 'bold',
        fontFamily: 'Courier New'
    },
    textContainer:{
        flex :1,
        justifyContent: 'center',
        alignContent: 'center',
        textAlign: 'center',
    },
    infoContainer:{
        flex:2,
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    statisticsView:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        margin:20,
    },
    score:{
        justifyContent: 'center',
        backgroundColor: colors.infoColor,
        paddingLeft: 15,
        paddingRight:15,
        paddingTop:10,
        paddingBottom:10,
        borderRadius: 10,
    },
    whiteText:{
        fontSize: RFValue(15),
        color:colors.brightText,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    buttonView:{
        alignContent:'flex-end',
        justifyContent:'flex-end',
        marginRight:20,
    },
    buttonNewGame:{
        alignSelf:'flex-end',
        padding:10,
        backgroundColor: colors.playField,
        borderRadius: 10,
        justifyContent: 'center',
        alignContent: 'center',
    },
});

export default styles;
