import {connect} from "react-redux";
import React, {Component} from 'react';
import {View, Text, TouchableOpacity, AsyncStorage} from 'react-native';
import Dialog from "react-native-dialog";
import styles from "./content/headerStyle";
import commonStyles from '../../Common/styles'
import generateEmptyBoard from "../../Utils/EmptyTableGenerator";
import {startNewGame, updateCells} from "../../Components/actions";
import dialogStyle from "../../Common/dialogStyle";


type Props = {
    score: number,
    startNewGame: () => void,
    updateCells: (cells: Cell[][]) => void
}

const InitialState = {
    modalVisibility: false,
    highScore: 0,
};

type State = typeof InitialState;

class Header extends Component<Props, State> {
    state = InitialState;

    componentWillMount() {
        this.createHighScore();
    }

    createHighScore = async () => {
        try {
            let highScore = await AsyncStorage.getItem('highScore');
            if (!highScore) {
                await AsyncStorage.setItem('highScore', '0');
                highScore = '0';
            }
            this.setState({highScore: Number(highScore)});
        } catch (e) {
            console.log(e);
        }
    };

    updateHighScore = async () => {
        if (this.props.score > this.state.highScore) {
            await AsyncStorage.setItem('highScore', this.props.score.toString());
            this.setState({highScore: this.props.score});
        }
    };

    changeModalVisibility = () => {
        this.setState({
            modalVisibility: !this.state.modalVisibility
        });
    };

    newGame = () => {
        this.props.startNewGame();
        this.props.updateCells(generateEmptyBoard());
        this.changeModalVisibility();
    };


    render() {
        this.updateHighScore();
        return (
            <View style={styles.header}>
                <View style={styles.textContainer}>
                    <Text style={styles.gameName}>
                        2048
                    </Text>
                </View>
                <View style={styles.infoContainer}>
                    <View style={styles.statisticsView}>
                        <View style={[styles.score, commonStyles.shadow]}>
                            <Text style={styles.whiteText}>
                                {`Your score \n ${this.props.score}`}
                            </Text>
                        </View>
                        <View style={[styles.score, commonStyles.shadow]}>
                            <Text style={styles.whiteText}>
                                {`Your score \n ${this.state.highScore}`}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.buttonView}>
                        <TouchableOpacity
                            onPress={this.changeModalVisibility}
                            style={[styles.buttonNewGame, commonStyles.shadow]}>
                            <Text style={styles.whiteText}>
                                New Game
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                < Dialog.Container visible={this.state.modalVisibility}>
                    < Dialog.Title style={dialogStyle.title}> Do you want to start new game?</Dialog.Title>
                    <Dialog.Button label="Yes" onPress={this.newGame}/>
                    <Dialog.Button label="No" onPress={this.changeModalVisibility}/>
                </Dialog.Container>
            </View>
        );
    }
}


type StateProps = {
    gameBoard: {
        score: number;
    };
};
const mapStateToProps = ({gameBoard: {score}}: StateProps) => ({
    score
});
const mapDispatchToProps = {
    startNewGame,
    updateCells
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Header);

