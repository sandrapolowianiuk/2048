import {StyleSheet} from "react-native";
import colors from "../../Common/colors"

const styles= StyleSheet.create({
    container:{
        backgroundColor: colors.background,
        flex :1
    },
    footer:{
        flex:1,
    }
});

export default styles;
